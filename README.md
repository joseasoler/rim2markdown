Rim2Markdown
===

[![RimWorld](https://img.shields.io/badge/RimWorld-1.4-informational)](https://rimworldgame.com/) [![Steam Downloads](https://img.shields.io/steam/downloads/2914759329)](https://steamcommunity.com/sharedfiles/filedetails/?id=ToDo) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)

Export in-game descriptions of [RimWorld](https://rimworldgame.com/) animals, plants, weapons, apparel and items as [Markdown](https://www.markdownguide.org/basic-syntax/) files. This format is supported by many online wikis such as [GitHub](https://github.com/), so you can easily use this tool to create a wiki for your mod. You can also keep them as an offline wiki of your modlist for your own use.

### How to use the mod

1) Rim2Markdown generates files for every mod in your modlist. Choose the mods you want to document, and launch the game with just them.
2) Enable development mode.
3) Start a dev quicktest game.
4) Open the debug actions menu.
5) In the Rim2Markdown category, press the Generate Markdown button.
6) Wait. Wait some more.
7) When the game finally starts working again, disregard the absolute mess created in the map.
8) Your Markdown files are ready!

The mod will generate a separate folder for each one of your mods. Each folder will contain separate markdown files for animals, plants, weapons and so on.

On Linux, the generated files can be found at /tmp/rim2markdown. On Windows 10, they can be found at C:\Users\<YOUR_USER>\AppData\Local\Temp\rim2markdown.

By default, the pages will link to images from your own downloaded RimWorld mods. You will need to fix the links manually if you plan to upload the Markdown files to the internet, but if your textures are uploaded as well this usually requires only a text replace on each file.

### Mod Compatibility

Rim2Markdown shows the same data as the in-game "View information" window. Any extra information added by mods in your modlist will show up as well. As a result of this, Rim2Markdown should be compatible with all mods.

### Frequently Asked Questions

**Q: Can I see examples of the results?**

A: You can find some in the following pages of the wiki for the [Biomes! Prehistoric](https://steamcommunity.com/sharedfiles/filedetails/?id=2860715703) mod:

* https://github.com/biomes-team/BiomesPrehistoric/wiki/Animals
* https://github.com/biomes-team/BiomesPrehistoric/wiki/Plants
* https://github.com/biomes-team/BiomesPrehistoric/wiki/Things
* https://github.com/biomes-team/BiomesPrehistoric/wiki/Trophies

Development
---

To compile this mod on Windows, you will need to install the [.NET Framework 4.7.2 Developer Pack](https://dotnet.microsoft.com/en-us/download/dotnet-framework/net472). On Linux the packages you need vary depending on your distribution of choice. Dependencies are managed using NuGet. Your checkout must be placed in the RimWorld/Mods folder to let it find the RimWorld assemblies required for compilation.

Contributions
---

This project encourages community involvement and contributions. Check the [CONTRIBUTING](CONTRIBUTING.md) file for details. Existing contributors can be checked in the [contributors list](https://gitlab.com/joseasoler/rim2markdown/-/graphs/main).

License
---

This project is licensed under the MIT license. Check the [LICENSE](LICENSE) file for details.

Acknowledgements
---

Read the [ACKNOWLEDGEMENTS](ACKNOWLEDGEMENTS.md) file for details.
