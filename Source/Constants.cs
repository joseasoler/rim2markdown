namespace RM
{
	public class Constants
	{
		/// <summary>
		/// Subfolder inside of the temp folder in which the results will be stored.
		/// </summary>
		public const string Folder = "rim2markdown";
	}
}