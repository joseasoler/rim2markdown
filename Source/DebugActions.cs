using System.Collections.Generic;
using Verse;

namespace RM
{
	public class DebugActions
	{
		/// <summary>
		/// Category to use for Rim2Markdown debug actions.
		/// </summary>
		private const string DebugCategory = "Rim2Markdown";

		private static readonly List<ThingCategory> ForbiddenThingCategories = new List<ThingCategory>
		{
			ThingCategory.Ethereal,
			ThingCategory.Mote,
			ThingCategory.Projectile
		};

		/// <summary>
		/// Generate markdown for all supported entities.
		/// </summary>
		[DebugAction(DebugCategory, allowedGameStates = AllowedGameStates.PlayingOnMap)]
		private static void GenerateMarkdown()
		{
			Animals.ToMarkdown();
			Things.ToMarkdown(def => def.plant != null, "plants.md");
			Things.ToMarkdown(def => def.IsApparel, "apparel.md");
			Things.ToMarkdown(def => def.IsWeapon, "weapons.md");
			Things.ToMarkdown(def => def.building != null, "buildings.md");
			Things.ToMarkdown(
				def => def.plant == null && def.race == null && !def.IsCorpse && !def.IsBlueprint && !def.IsWeapon &&
				       !def.IsApparel && def.skyfaller == null && def.mote == null &&
				       !ForbiddenThingCategories.Contains(def.category),
				"things.md");
		}
	}
}