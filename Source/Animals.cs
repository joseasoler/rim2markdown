using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RimWorld;
using Verse;

namespace RM
{
	public static class Animals
	{
		public static void ToMarkdown()
		{
			var defsByPackageId = new Dictionary<string, List<PawnKindDef>>();

			// Classify animals by mod.
			foreach (var pawnDef in DefDatabase<PawnKindDef>.AllDefsListForReading)
			{
				var packageId = pawnDef.modContentPack?.PackageId;
				if (packageId == null || pawnDef.race?.race == null || !pawnDef.race.race.Animal)
				{
					continue;
				}

				if (!defsByPackageId.ContainsKey(packageId))
				{
					defsByPackageId[packageId] = new List<PawnKindDef>();
				}

				defsByPackageId[packageId].Add(pawnDef);
			}

			var tmpPath = Path.Combine(Path.GetTempPath(), Constants.Folder);
			foreach (var packageIdDefs in defsByPackageId)
			{
				var modPath = Path.Combine(tmpPath, packageIdDefs.Key);
				Directory.CreateDirectory(modPath);

				packageIdDefs.Value.Sort((c1, c2) =>
					string.Compare(c1.LabelCap.ToString(), c2.LabelCap.ToString(), StringComparison.Ordinal));

				using (var writer = new StreamWriter(Path.Combine(modPath, "animals.md"), false))
				{
					foreach (var def in packageIdDefs.Value)
					{
						var newPawn = PawnGenerator.GeneratePawn(def, Faction.OfPlayer);
						GenSpawn.Spawn(newPawn, Find.CurrentMap.Center, Find.CurrentMap);
						// See StatsReportUtility.DrawStatsReport
						var entries = new List<StatDrawEntry>();
						entries.AddRange(newPawn.def.SpecialDisplayStats(StatRequest.For(newPawn)));
						entries.AddRange(StatsReportUtility.StatsToDraw(newPawn).Where(r => r.ShouldDisplay));
						entries.RemoveAll(de => de.stat != null && !de.stat.showNonAbstract);
						entries.RemoveAll(de => de.category == StatCategoryDefOf.Source);
						entries = entries.OrderBy(sd => sd.category.displayOrder)
							.ThenByDescending(sd => sd.DisplayPriorityWithinCategory).ThenBy(sd => sd.LabelCap).ToList();
						newPawn.Destroy();

						writer.WriteLine();
						writer.WriteLine($"## {def.LabelCap}");
						writer.WriteLine();

						if (def.lifeStages != null && def.lifeStages.Count > 0)
						{
							var splitPath = def.lifeStages.Last().bodyGraphicData.texPath.Split('/');
							var pathEnd = splitPath[splitPath.Length - 1] + "_east.png";
							var absolutePath = TextureCache.FromMod(def.modContentPack).Find(texture => texture.EndsWith(pathEnd));
							writer.WriteLine($"![{def.LabelCap}]({absolutePath} \"{def.LabelCap}\")");
							writer.WriteLine();
						}

						writer.WriteLine($"{def.race.description}");
						writer.WriteLine();
						// See StatsReportUtility.DrawStatsWorker.
						string categoryLabel = null;
						foreach (var entry in entries)
						{
							if (entry.category.LabelCap != categoryLabel)
							{
								categoryLabel = entry.category.LabelCap;
								writer.WriteLine($"### {categoryLabel}");
								writer.WriteLine();
							}

							if (entry.labelInt != "Description".Translate())
							{
								writer.WriteLine($"**{entry.LabelCap}:** {entry.ValueString}");
								writer.WriteLine();
							}
						}
					}
				}
			}
		}
	}
}