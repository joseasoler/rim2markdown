using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace RM
{
	public class TextureCache
	{
		/// <summary>
		/// Lazily initialized cache of textures for each mod.
		/// </summary>
		private static Dictionary<string, List<string>> _textureMap;

		public static List<string> FromMod(ModContentPack mod)
		{
			if (_textureMap == null)
			{
				_textureMap = new Dictionary<string, List<string>>();
			}

			if (!_textureMap.ContainsKey(mod.PackageId))
			{
				_textureMap[mod.PackageId] = ModContentPack
					.GetAllFilesForMod(mod, GenFilePaths.ContentPath<Texture2D>(),
						ModContentLoader<Texture2D>.IsAcceptableExtension).Select(fileInfo => fileInfo.Value.FullName).ToList();
			}

			return _textureMap[mod.PackageId];
		}
	}
}