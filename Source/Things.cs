using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RimWorld;
using UnityEngine;
using Verse;

namespace RM
{
	public static class Things
	{
		private static readonly HashSet<string> ExclusionList = new HashSet<string>
		{
			// Placing the Black Hive Mound hangs the game. Since Rim2Markdown is not placing things in a standard way and the
			// info of this building is not that relevant, it is just ignored.
			"AA_BlackHiveMound"
		};

		public static void ToMarkdown(Func<ThingDef, bool> pred, string filename)
		{
			var defsByPackageId = new Dictionary<string, List<ThingDef>>();

			// Classify defs by mod.
			foreach (var def in DefDatabase<ThingDef>.AllDefsListForReading)
			{
				var packageId = def.modContentPack?.PackageId;
				if (packageId == null || !pred(def))
				{
					continue;
				}

				if (!defsByPackageId.ContainsKey(packageId))
				{
					defsByPackageId[packageId] = new List<ThingDef>();
				}

				defsByPackageId[packageId].Add(def);
			}

			var tmpPath = Path.Combine(Path.GetTempPath(), Constants.Folder);
			foreach (var packageIdDefs in defsByPackageId)
			{
				var modPath = Path.Combine(tmpPath, packageIdDefs.Key);
				Directory.CreateDirectory(modPath);

				packageIdDefs.Value.Sort((c1, c2) =>
					string.Compare(c1.LabelCap.ToString(), c2.LabelCap.ToString(), StringComparison.Ordinal));

				using (var writer = new StreamWriter(Path.Combine(modPath, filename), false))
				{
					foreach (var def in packageIdDefs.Value)
					{
						if (def.LabelCap.NullOrEmpty() || def.defName.StartsWith("Frame_") || ExclusionList.Contains(def.defName))
						{
							continue;
						}

						try
						{
							var thing = ThingMaker.MakeThing(def, GenStuff.DefaultStuffFor(def));
							thing.TryGetComp<CompQuality>()?.SetQuality(QualityCategory.Normal, ArtGenerationContext.Colony);

							if (thing is MinifiedThing)
							{
								continue;
							}

							if (thing.def.CanHaveFaction)
							{
								if (thing.def.building != null && thing.def.building.isInsectCocoon)
								{
									thing.SetFaction(Faction.OfInsects);
								}
								else
								{
									thing.SetFaction(Faction.OfPlayerSilentFail);
								}
							}

							Thing wall = null;
							if (def.building != null && def.building.canPlaceOverWall)
							{
								// Place a temporary wall for air vents and similar buildings.
								wall = ThingMaker.MakeThing(ThingDefOf.Wall, GenStuff.DefaultStuffFor(ThingDefOf.Wall));
								GenPlace.TryPlaceThing(wall, Find.CurrentMap.Center, Find.CurrentMap, ThingPlaceMode.Near);
							}

							GenPlace.TryPlaceThing(thing, Find.CurrentMap.Center, Find.CurrentMap, ThingPlaceMode.Near);

							// See StatsReportUtility.DrawStatsReport
							var entries = new List<StatDrawEntry>();

							entries.AddRange(thing.def.SpecialDisplayStats(StatRequest.For(thing)));
							entries.AddRange(StatsReportUtility.StatsToDraw(thing).Where(r => r.ShouldDisplay));
							entries.RemoveAll(de => de.stat != null && !de.stat.showNonAbstract);
							entries.RemoveAll(de => de.category == StatCategoryDefOf.Source);
							entries = entries.OrderBy(sd => sd.category.displayOrder)
								.ThenByDescending(sd => sd.DisplayPriorityWithinCategory).ThenBy(sd => sd.LabelCap).ToList();

							if (!thing.Destroyed && thing.def.destroyable)
							{
								thing.Destroy();
							}

							wall?.Destroy();

							writer.WriteLine();
							writer.WriteLine($"## {def.LabelCap}");
							writer.WriteLine();

							if (def.graphicData?.texPath != null)
							{
								var splitPath = def.graphicData.texPath.Split('/');
								var absolutePath = TextureCache.FromMod(def.modContentPack)
									.Find(texture => texture.EndsWith(splitPath[splitPath.Length - 1] + ".png"));
								if (!string.IsNullOrEmpty(absolutePath))
								{
									writer.WriteLine($"![{def.LabelCap}]({absolutePath} \"{def.LabelCap}\")");
									writer.WriteLine();
								}
							}

							writer.WriteLine($"{def.description}");
							writer.WriteLine();
							// See StatsReportUtility.DrawStatsWorker.
							string categoryLabel = null;
							foreach (var entry in entries)
							{
								if (entry.category.LabelCap != categoryLabel)
								{
									categoryLabel = entry.category.LabelCap;
									writer.WriteLine($"### {categoryLabel}");
									writer.WriteLine();
								}

								if (entry.labelInt != "Description".Translate())
								{
									writer.WriteLine($"**{entry.LabelCap}:** {entry.ValueString}");
									writer.WriteLine();
								}
							}
						}
						catch (Exception exc)
						{
							if (Prefs.DevMode)
							{
								Log.Error($"Exception while processing {def.defName}: {exc}");
							}
						}
					}
				}
			}
		}
	}
}